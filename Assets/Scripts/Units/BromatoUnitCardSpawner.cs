using System.Collections.Generic;
using UnityEngine;

public class BromatoUnitCardSpawner : Timer
{
    private readonly System.Random _rnd = new();
    public override float Threshold { get; set; } = TweakData.BattleSystem.CARD_SPAWNER_TIMER;

    [SerializeField] private BromatoUnitManager _unitManager;
    [SerializeField] private Player _player;
    [SerializeField] private Vector2 _spawnPosition;

    [SerializeField] internal List<Vector3> CardSlots = new();
    internal List<BromatoUnitController> Cards = new();

    public override void OnTick()
    {
        if (Cards.Count < CardSlots.Count)
        {

            Cards.Add(_unitManager.SpawnPlayerUnit(_unitManager.PlayerUnits[_rnd.Next(_unitManager.PlayerUnits.Count)], _player, _spawnPosition));
        }
    }
}
