using UnityEngine;

public class BromatoUnitEnemySpawner : Timer
{
    public override float Threshold { get; set; } = TweakData.BattleSystem.ENEMY_SPAWNER_TIMER;

    public BromatoUnitManager UnitManager;

    public override void OnTick()
    {
        UnitManager.SpawnEnemyUnit(
            UnitManager.EnemyUnits[Random.Range(0, UnitManager.EnemyUnits.Count - 1)],
            TweakData.Boundaries.RIGHT_BORDER,
            Random.Range(TweakData.Boundaries.ENEMY_SPAWNER_Y_MIN, TweakData.Boundaries.ENEMY_SPAWNER_Y_MAX)
        );
        Threshold *= .9999999999f;
    }
}
