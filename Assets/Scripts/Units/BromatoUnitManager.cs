using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class BromatoUnitManager : MonoBehaviour
{
    private readonly List<BromatoUnitController> _spawnedPlayerUnits = new();
    private readonly List<BromatoUnitController> _spawnedEnemyUnits = new();

    [SerializeField] internal List<GameObject> PlayerUnits;
    [SerializeField] internal List<GameObject> EnemyUnits;

    [SerializeField] private Game _game;
    [SerializeField] private BromatoUnitCardSpawner _cardSpawner;
    [SerializeField] private RectTransform _buyArea;

    internal BromatoUnitController SpawnPlayerUnit(GameObject prefab, Player owner, float x, float y)
    {
        return SpawnPlayerUnit(prefab, owner, new Vector2(x, y));
    }

    internal BromatoUnitController SpawnPlayerUnit(GameObject prefab, Player owner, Vector2 pos)
    {
        BromatoUnitController unit = SpawnUnit(prefab, pos, owner);
        _spawnedPlayerUnits.Add(unit);
        return unit;
    }

    internal BromatoUnitController SpawnEnemyUnit(GameObject prefab, float x, float y)
    {
        return SpawnEnemyUnit(prefab, new Vector2(x, y));
    }

    internal BromatoUnitController SpawnEnemyUnit(GameObject prefab, Vector2 pos)
    {
        BromatoUnitController unit = SpawnUnit(prefab, pos);
        _spawnedEnemyUnits.Add(unit);
        return unit;
    }

    private BromatoUnitController SpawnUnit(GameObject prefab, Vector2 pos, Player owner = null)
    {
        BromatoUnitController unit = Instantiate(prefab, pos, Quaternion.identity, _game.transform)
                                     .GetComponent<BromatoUnitController>();
        unit.Setup(this, _cardSpawner, _game, owner, _buyArea);
        return unit;
    }

    internal void DespawnUnit(BromatoUnitController bromato, bool isDeath = false)
    {
        if (_spawnedEnemyUnits.Contains(bromato)) _spawnedEnemyUnits.Remove(bromato);
        else if (_spawnedPlayerUnits.Contains(bromato)) _spawnedPlayerUnits.Remove(bromato);

        if (DOTween.IsTweening(bromato.transform, true))
        {
            DOTween.Pause(bromato.transform);
        }

        if (isDeath)
        {
            SpawnCorpse(bromato.transform.position);
        }

        Destroy(bromato.gameObject);
    }

    internal void Clear()
    {
        _spawnedPlayerUnits.ForEach(u => DespawnUnit(u));
        _spawnedEnemyUnits.ForEach(u => DespawnUnit(u));
    }

    internal void SpawnCorpse(Vector3 pos)
    {
        // TODO spawn corpse
    }
}