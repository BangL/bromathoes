using System;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TMPro;
using UnityEngine;
using UnityHFSM;

public class BromatoUnitController : MonoBehaviour
{
    private StateMachine _fsm;
    private StateMachine _cardFSM;
    private StateMachine _aliveFsm;
    private Game _game;
    private Player _owner;
    private RectTransform _buyArea;
    private bool _isBought;
    private float _maxHealthScale;
    private int _health;
    private BromatoUnitController _target;
    private TweenerCore<Vector3, Vector3, VectorOptions> _tweener;
    private BromatoUnitManager _unitManager;
    private BromatoUnitCardSpawner _cardSpawner;
    private Vector3 _currentSlotPos;
    private int _currentSlotIndex;

    [SerializeField] private Transform _healthBar;
    [SerializeField] private TMP_Text _statusLabel;
    [SerializeField] private int _maxHealth;
    [SerializeField] private bool _isEnemy;
    [SerializeField] private float _speed;
    [SerializeField] private float _attackSpeed;
    [SerializeField] private int _attackDamage;
    [SerializeField] private bool _attackStuns;
    [SerializeField] private int _moneyValue;

    void Awake()
    {
        _maxHealthScale = _healthBar.localScale.x;
        _health = _maxHealth;
        UpdateHealthBar();
    }

    internal void Setup(BromatoUnitManager unitManager, BromatoUnitCardSpawner cardSpawner, Game game, Player owner, RectTransform buyArea)
    {
        _unitManager = unitManager;
        _cardSpawner = cardSpawner;
        _game = game;
        _owner = owner;
        _buyArea = buyArea;
    }

    void Start()
    {
        _fsm = new();

        // START

        _fsm.AddState("Start", isGhostState: true);
        _fsm.SetStartState("Start");

        // CARD

        _cardFSM = new();
        _fsm.AddState("Card", _cardFSM);
        _fsm.AddTransition("Start", "Card", transition => !_isEnemy);

        // CARD / SPAWN

        _cardFSM.AddState("Spawn",
            onEnter: state =>
            {
                // TODO: show card bg
                // TODO: show unit info
                // TODO: fade into right most slot, canExit when animation is done
            }
        );
        _cardFSM.SetStartState("Spawn");

        // CARD / HOMING

        _cardFSM.AddState("Homing",
            onEnter: state =>
            {
                _currentSlotIndex = _cardSpawner.Cards.IndexOf(this);
                // move to current tray slot
                _currentSlotPos = _cardSpawner.CardSlots[_currentSlotIndex];
                if (!transform.position.Equals(_currentSlotPos))
                {
                    _tweener = transform.DOMove(_currentSlotPos, .5f, false);
                }
                else
                {
                    _fsm.StateCanExit();
                }
            },
            onExit: state =>
            {
                if (_tweener != null)
                {
                    DOTween.Kill(transform, false);
                }
            }
        );
        _cardFSM.AddTransition("Spawn", "Homing");
        _cardFSM.AddTransition("Homing", "Idle", transition => _currentSlotPos.Equals(transform.position));

        // CARD / IDLE

        _cardFSM.AddState("Idle", isGhostState: true);
        _cardFSM.AddTransition("Idle", "Homing", transition => !_currentSlotPos.Equals(transform.position) || !_currentSlotIndex.Equals(_cardSpawner.Cards.IndexOf(this)));

        // CARD / DRAG

        _cardFSM.AddState("Dragged",
            onLogic: state =>
            {
                if (state.timer.Elapsed >= .002f)
                {
                    state.timer.Reset();

                    // follow mouse
                    transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
            },
            onExit: state =>
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(_buyArea, Input.mousePosition) && _owner.Wallet().TryPay(_moneyValue))
                {
                    _isBought = true;
                }
            }
        );
        _cardFSM.AddTriggerTransitionFromAny("OnDrag", "Dragged", forceInstantly: true);
        _cardFSM.AddTriggerTransition("OnMouseUp", "Dragged", "Idle", transition => !_isBought);
        _cardFSM.AddTriggerTransition("OnMouseUp", "Dragged", "Bought", transition => _isBought); // FIXME ???

        // BUY

        _fsm.AddState("Bought",
            onEnter: state =>
            {
                _cardSpawner.Cards.Remove(this);
                // TODO: hide card bg
                // TODO: hide unit info
            }
        );
        _fsm.AddTransition("Card", "Bought", transition => _isBought); // FIXME ???

        // DECAYED

        _fsm.AddState("Decayed",
            onEnter: state =>
            {
                _cardSpawner.Cards.Remove(this);
                // TODO: fade out animation
            }
        );
        _fsm.AddTransition(new TransitionAfter("Card", "Decayed", TweakData.BattleSystem.CARD_DURATION));

        // ALIVE

        _aliveFsm = new();
        _fsm.AddState("Alive", _aliveFsm);
        _fsm.AddTransition("Start", "Alive", transition => _isEnemy);
        _fsm.AddTransition("Bought", "Alive");

        // ALIVE / RUNNING

        _aliveFsm.AddState("Running",
            onEnter: state =>
            {
                // start run animation
                Animator animator = GetComponentInChildren<Animator>();
                if (animator != null)
                {
                    animator.SetTrigger("OnRunning");
                }

                float border = _isEnemy ? TweakData.Boundaries.LEFT_BORDER : TweakData.Boundaries.RIGHT_BORDER;
                _tweener = transform.DOMoveX(border, _speed, false)
                    .SetSpeedBased(true)
                    .SetEase(Ease.Linear)
                    .OnComplete(() =>
                    {
                        if (_isEnemy)
                        {
                            // enemy unit reached left border, game is lost
                            //_game.GameLost();
                            //return;
                        }

                        // player unit reached right border, recycle unit
                        if (_owner != null) _owner.Wallet().Receive((int)Math.Round(_moneyValue * TweakData.Economy.UNIT_RECYCLE_FACTOR));

                        // despawn
                        Despawn(false);
                    });
            },
            onExit: state =>
            {
                if (_tweener != null)
                {
                    DOTween.Kill(transform, false);
                }
            }
        );
        _aliveFsm.SetStartState("Running");

        // ALIVE / FIGHTING

        _aliveFsm.AddState("Fighting",
            onEnter: state =>
            {
                // start fighting animation
                Animator animator = GetComponentInChildren<Animator>();
                if (animator != null)
                {
                    animator.SetTrigger("OnFighting");
                }
            },
            onLogic: state =>
            {
                if (state.timer.Elapsed >= _attackSpeed)
                {
                    state.timer.Reset();

                    // TODO: insert outgoing hit (attack) sound
                    //GetComponent<AudioSource>().Play();

                    _target.ReceiveHit(_attackDamage, _attackStuns);

                    if (!_target.IsAlive())
                    {
                        // reward player for killing enemy
                        if (_target._isEnemy && _owner != null)
                        {
                            _owner.Wallet().Receive(_target._moneyValue);
                        }
                    }
                }
            }
        );
        _aliveFsm.AddTwoWayTransition("Running", "Fighting", transition => HasTarget());

        // ALIVE / STUNNED

        _aliveFsm.AddState("Stunned",
            onEnter: state =>
            {
                // TODO?: insert stun sound
                //GetComponent<AudioSource>().Play();
            },
            canExit: state => state.timer.Elapsed >= TweakData.BattleSystem.UNIT_STUN_DURATION,
            needsExitTime: true
        );
        _aliveFsm.AddTriggerTransitionFromAny("OnStun", "Stunned", forceInstantly: true);
        _aliveFsm.AddTransition("Stunned", "Running", transition => !HasTarget());
        _aliveFsm.AddTransition("Stunned", "Fighting", transition => HasTarget());

        // DEAD

        _fsm.AddState("Dead",
            onEnter: state =>
            {
                // TODO: insert die sound
                //GetComponent<AudioSource>().Play();

                // TODO: die anim

                Despawn(true); // TODO?: recycle
            }
        );
        _fsm.AddTriggerTransition("OnHit", "Alive", "Dead", transition => _health <= 0, forceInstantly: true);

        // DESPAWN

        _fsm.AddState("Despawn",
            onEnter: state =>
            {
                Despawn(false); // TODO?: recycle
            }
        );
        _fsm.AddTransition("Decayed", "Despawn");

        _fsm.Init();
    }

    private void ReceiveHit(int attackDamage, bool attackStuns)
    {
        _health -= attackDamage;
        UpdateHealthBar();
        _fsm.Trigger("OnHit");

        if (IsAlive())
        {
            // play incoming hit sound
            GetComponent<AudioSource>().Play();

            // TODO?: play hit animation?

            if (attackStuns)
            {
                _fsm.Trigger("OnStun");
            }
        }
    }

    void Update()
    {
        _fsm.OnLogic();
        _statusLabel.text = _fsm.GetActiveHierarchyPath();
    }

    void OnMouseDrag()
    {
        _fsm.Trigger("OnDrag");
    }

    void OnMouseUp()
    {
        _fsm.Trigger("OnMouseUp");
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        TrySetTarget(other);
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        TrySetTarget(other);
    }

    private bool TrySetTarget(Collider2D other)
    {
        if (other.gameObject.TryGetComponent(out BromatoUnitController otherBromato))
        {
            return TrySetTarget(otherBromato);
        }

        return false;
    }

    private bool TrySetTarget(BromatoUnitController target)
    {
        if ((_target == null || !_target.IsAlive()) && target._isEnemy != _isEnemy && target.IsAlive())
        {
            _target = target;
            return true;
        }

        return false;
    }

    private BromatoUnitController GetTarget()
    {
        if (null != _target && _target.IsAlive())
        {
            return _target;
        }

        return null;
    }

    private bool HasTarget()
    {
        return GetTarget() != null;
    }

    private bool IsAlive()
    {
        return _fsm.ActiveStateName == "Alive";
    }

    private void UpdateHealthBar()
    {
        _healthBar.localScale = new Vector3(_maxHealthScale * GetHealthPercentage(), _healthBar.localScale.y, _healthBar.localScale.z);
    }

    private float GetHealthPercentage()
    {
        return 1f / _maxHealth * _health;
    }

    internal void Despawn(bool isDeath)
    {
        _unitManager.DespawnUnit(this, isDeath);
    }

}