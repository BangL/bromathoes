using System;
using TMPro;
using UnityEngine;

public class PlayerWallet : MonoBehaviour
{

    private int _money;

    public TextMeshProUGUI MoneyLabel;

    void Awake()
    {
        Reset();
    }

    internal void Reset()
    {
        _money = 0;
        ChangeMoney(TweakData.Economy.START_MONEY_AMOUNT);
    }

    internal void Receive(int value)
    {
        ChangeMoney(Math.Abs(value));
    }

    internal bool TryPay(int price)
    {
        if (_money < price)
        {
            return false;
        }

        ChangeMoney(-Math.Abs(price));

        return true;
    }

    private void ChangeMoney(int value)
    {
        _money += value;
        UpdateGUI();
    }

    private void UpdateGUI()
    {
        MoneyLabel.text = _money.ToString();
    }
}