public class PlayerPaydayTimer : Timer
{
    public override float Threshold { get; set; } = TweakData.Economy.PAYDAY_TIMER;

    public Player Player;

    public override void OnTick()
    {
        Player.Wallet().Receive(TweakData.Economy.PAYDAY_AMOUNT);
    }
}