using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerWallet _wallet;

    internal PlayerWallet Wallet()
    {
        return _wallet;
    }
}
