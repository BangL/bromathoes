public static class TweakData
{
    public static class Economy
    {
        // Timers
        internal const float PAYDAY_TIMER = 1f;

        // Income
        internal const int START_MONEY_AMOUNT = 10;
        internal const int PAYDAY_AMOUNT = 1;
        internal const float UNIT_RECYCLE_FACTOR = .5f;

    }

    public static class BattleSystem
    {
        // Enemy Spawner
        internal const float ENEMY_SPAWNER_TIMER = 3;
        internal const float UNIT_STUN_DURATION = .2f;

        // Card Spawner
        internal const float CARD_SPAWNER_TIMER = 3;
        internal const float CARD_DURATION = 10;
    }

    public static class Boundaries
    {
        internal const float ENEMY_SPAWNER_Y_MIN = -2;
        internal const float ENEMY_SPAWNER_Y_MAX = 4;
        internal const float LEFT_BORDER = -10f;
        internal const float RIGHT_BORDER = 10f;
    }
}