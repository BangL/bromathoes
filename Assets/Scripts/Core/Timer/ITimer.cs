public interface ITimer
{
    public abstract float Threshold { get; set; }
    public abstract bool StartPaused { get; }
    public abstract bool IsOneShot { get; }

    public abstract void Reset();
    public abstract void Stop();
    public abstract void Pause(bool reset);
    public abstract void Resume();
    public abstract void OnTick();
}