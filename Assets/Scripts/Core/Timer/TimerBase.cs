using UnityEngine;

public abstract class Timer : MonoBehaviour, ITimer
{
    private float _currentTime;
    private float _currentThreshold;

    private bool _isDone = false;
    private bool _isPaused = false;

    public abstract float Threshold { get; set; }
    public virtual bool StartPaused { get; } = false;
    public virtual bool IsOneShot { get; } = false;

    protected virtual void Awake()
    {
        _isPaused = StartPaused;
        Reset();
    }

    protected virtual void Update()
    {
        if (_isPaused || _isDone) return; // skip if stopped/paused/done

        _currentTime += Time.deltaTime;

        if (_currentTime >= _currentThreshold) // threshold reached
        {
            if (IsOneShot) _isDone = true; // set oneshot done
            Reset();
            OnTick();
        }
    }

    public virtual void Reset()
    {
        _currentTime = 0f;
        _currentThreshold = Threshold;
    }

    public virtual void Stop()
    {
        if (!_isDone)
        {
            _isDone = true;
        }
    }

    public virtual void Pause(bool reset)
    {
        if (!_isPaused)
        {
            _isPaused = true;
            if (reset) Reset();
        }
    }

    public virtual void Resume()
    {
        if (_isPaused)
        {
            _isPaused = false;
        }
    }

    public abstract void OnTick();
}