using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] private BromatoUnitEnemySpawner _enemySpawner;
    [SerializeField] private BromatoUnitManager _unitManager;
    [SerializeField] private PlayerPaydayTimer _playerPaydayTimer;

    internal void GameLost()
    {
        _playerPaydayTimer.Stop();
        _enemySpawner.Stop();
        _unitManager.Clear();
        // TODO: game lost label
    }
}
